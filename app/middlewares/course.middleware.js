const courseRouterMiddleware = (req, res, next) => {
    console.log("Course Request URL: ", req.url);
    
    next();
}

const courseGetAllMiddleware = (req, res, next) => {
    console.log("Course Get ALL Middleware");
    
    next();
}

const courseGetDetailMiddleware = (req, res, next) => {
    console.log("Course Get Detail Middleware");
    
    next();
}

const courseCreateMiddleware = (req, res, next) => {
    console.log("Course Create Middleware");
    
    next();
}

const courseUpdateMiddleware = (req, res, next) => {
    console.log("Course Update Middleware");
    
    next();
}

const courseDeleteMiddleware = (req, res, next) => {
    console.log("Course Delete Middleware");
    
    next();
}


module.exports = {
    courseRouterMiddleware,
    courseGetAllMiddleware,
    courseGetDetailMiddleware,
    courseCreateMiddleware,
    courseUpdateMiddleware,
    courseDeleteMiddleware
}