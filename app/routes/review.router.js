// Import thư viện express
const express = require("express");

const router = express.Router();

// Import review middleware sang
const reviewMiddleware = require("../middlewares/review.middleware");

// router.use(reviewMiddleware.reviewRouterMiddleware);

// Get all
router.get("/reviews", reviewMiddleware.reviewGetAllMiddleware, (req, res) => {
    res.status(200).json({
        message: "Get all review"
    })
});

// Create
router.post("/reviews", reviewMiddleware.reviewCreateMiddleware, (req, res) => {
    res.status(200).json({
        message: "Create review"
    })
});

// Get detail
router.get("/reviews/:reviewid", reviewMiddleware.reviewGetDetailMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    res.status(200).json({
        message: "Get review detail id = " + reviewid
    })
});

// Update
router.put("/reviews/:reviewid", reviewMiddleware.reviewUpdateMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    res.status(200).json({
        message: "Update review id = " + reviewid
    })
});

// Delete
router.delete("/reviews/:reviewid", reviewMiddleware.reviewDeleteMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    res.status(200).json({
        message: "Delete review id = " + reviewid
    })
});

// Export router thành 1 module
module.exports = router;